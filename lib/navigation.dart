import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Navigation extends StatefulWidget {
  const Navigation({Key? key}) : super(key: key);

  @override
  State<Navigation> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Navigation"),
      ),
      body: Center(
        child: Text("Bienvenu dans le store magasin", style: GoogleFonts.allura(fontSize: 30),),
      ),
      bottomNavigationBar: ClipPath(

        clipper: CustomBottomNavigationBarClipper(),
        child:  BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFF421927),
          //currentIndex: _selectedIndex,
          onTap: (int index) {
            setState(() {

            });
          },
          items: <BottomNavigationBarItem>[

            BottomNavigationBarItem( // l'odre des propriété a de l'importance ici sinon ça ne passe pas
              label: "",
              icon: Icon(Icons.home, color: Colors.white,),
            ),
            BottomNavigationBarItem(
              label: "",
              icon: Icon(Icons.favorite_border_outlined, color: Colors.white,),
            ),
            BottomNavigationBarItem(
              label: "",
              icon: Icon(Icons.settings_outlined, color: Colors.white,),
            ),
            BottomNavigationBarItem(
              label: "",
              icon: Icon(Icons.person_pin, color: Colors.white,),
            ),
          ],
        ),
      )

    );
  }
}

class CustomBottomNavigationBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 60);
    path.quadraticBezierTo(size.width/4,size.height-90,size.width/2,size.height-60);
    path.quadraticBezierTo(size.width/4*3,size.height-30,size.width,size.height-60);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

