import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'addCredit.dart';
class GoogleMap extends StatefulWidget {
  const GoogleMap({Key? key}) : super(key: key);

  @override
  State<GoogleMap> createState() => _GoogleMapState();
}

class _GoogleMapState extends State<GoogleMap> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading:IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey,
            size: 29,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ) ,
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text("Checkout",style: GoogleFonts.aBeeZee(color: Colors.black),),
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: ListView(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Color(0xFFF4F4F4),
              ),
              child: Row(
                children: [
                  Expanded(
                    flex:1,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        primary: Color(0xFF421927),
                        onPrimary: Colors.white,
                        elevation: 3,
                        minimumSize: Size(172, 45), //////// HERE
                      ),
                      onPressed: () {
                      },
                      child: Text('Delivery'),
                    ),
                  ),
                  Expanded(
                    flex:1,
                    child:ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        primary: Color(0xFFF4F4F4),
                        onPrimary: Colors.black,
                        elevation: 0,
                        minimumSize: Size(172, 45), //////// HERE
                      ),
                      onPressed: () {
                      },
                      child: Text('Pickup'),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height:10),
            TextFormField(

                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.access_time,color: Color(0xFF421927),),
                  labelStyle: TextStyle(color: Colors.black),
                  labelText:("30-40 min"),
                )
            ),
            Container(
              margin: EdgeInsets.symmetric(),
              child: ListTile(
                leading:Icon(Icons.place_outlined,color: Color(0xFF421927)),
                title: Text("2342 W Cullerton St"),
                trailing: Icon(Icons.arrow_forward_ios_outlined),
              ),
            ),
            SizedBox(height:10),
            Container(
              width:400,
              height:235,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                    image: AssetImage('assets/images/carte.jpg'),
                    fit: BoxFit.cover
                ),
              ),
            ),
            SizedBox(height:20),
            TextFormField(
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color(0xFFF4F4F4),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(10),borderSide: BorderSide.none),
                  labelStyle: TextStyle(color: Colors.grey),
                  labelText:("Apartment number,floor,office"),
                )
            ),
            SizedBox(height:20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Price"),
                Text("\$494.95",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 15),)
              ],
            ),
            Container(
              padding: EdgeInsets.fromLTRB(40, 40, 40, 0),
              child:
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  primary: Color(0xFF421927),
                  onPrimary: Colors.white,
                  elevation: 3,
                  minimumSize: Size(270, 60), //////// HERE
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddCredit()));
                },
                child: Text('Proceed to Payments'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
