import 'package:flutter/material.dart';
import 'package:komido_boutique/panier.dart';

class DetailProd extends StatefulWidget {
  const DetailProd({Key? key}) : super(key: key);

  @override
  State<DetailProd> createState() => _DetailProdState();
}

class _DetailProdState extends State<DetailProd> {
  int _articleNumber = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading:IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey,
            size: 29,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ) ,
        backgroundColor: Color(0xFFFEF5F6),
        elevation: 0,
        actions: [
          Icon(Icons.favorite_border_outlined, color: Colors.black87,),
        ],
      ),
      body: Container(

        child: ListView(
          children: [

            Container( //BLOC 1
                decoration: BoxDecoration(color: Color(0xFFFEF5F6)),
                child: Stack(
                  children: [
                    Container(
                      padding: new EdgeInsets.fromLTRB(0, 30, 0, 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Image.asset("assets/bakivinroseAccent2.png", scale: 5,),
                        ],
                      ),
                    ),

                    Container(
                      padding: new EdgeInsets.all(20),
                      child: widArticleImage(),
                    ),
                  ],
                )

            ),

            Container( //BLOC 2
              padding: new EdgeInsets.all(20),
              child: widArticleDetail(),
            ),

            Container( //BLOC 3
              padding: new EdgeInsets.fromLTRB(20, 30, 20, 20),
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(
                          color: Color(0xFFF4DEE3),
                          width: 3.0
                      )
                  )
              ),
              child: widAddToCard(),
            ),
          ],
        ),
      ),
    );
  }

  Widget widAddToCard(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFF4DEE3),
                    borderRadius: BorderRadius.circular(17.0),
                  ),
                  child: IconButton(
                      onPressed: () {
                        setState(() {
                          _articleNumber--;
                        });
                      },
                      icon: Icon(Icons.remove,)
                  ),
                ),

                SizedBox(width: 20,),
                Text(_articleNumber.toString()),
                SizedBox(width: 20,),

                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFF4DEE3),
                    borderRadius: BorderRadius.circular(17.0),
                  ),
                  child: IconButton(
                      onPressed: () {
                        setState(() {
                          _articleNumber++;
                        });
                      },
                      icon: Icon(Icons.add)
                  ),
                ),
              ],
            ),
           // SizedBox(width: 20,),

            Row(
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                    primary: Color(0xFF421927),
                    onPrimary: Colors.white,
                    elevation: 3,
                    minimumSize: Size(172, 60), //////// HERE
                  ),
                  onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Panier()));
              },
                  
                  child: Text('Add to card', style:  TextStyle(fontSize: 15),),
                ),
              ],
            )
          ],
        )
      ],
    );
  }

  Widget widArticleDetail(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Description", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
            SizedBox(height: 15,),
            Text(
              "A delluge of rain in February meant we enterred"
                  "the growing season with saturated sails and a"
                  "full water table.",
              style: TextStyle(color: Colors.grey, fontSize: 12),)
          ],
        ),
        SizedBox(height: 25,),

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            Text("Nutrutional values", style: TextStyle(fontWeight: FontWeight.bold),),
            SizedBox(height: 15,),
            Row(
              children: [
                Text("sugar ", style: TextStyle(color: Colors.grey, fontSize: 12),),
                Text("5g/0.03 gal ", style: TextStyle(color: Colors.black54, fontSize: 12, fontWeight: FontWeight.bold),),
                Text("Calories ", style: TextStyle(color: Colors.grey, fontSize: 12),),
                Text("23kcal/0.0.3 gal", style: TextStyle(color: Colors.black54, fontSize: 12, fontWeight: FontWeight.bold),),
              ],
            )

          ],
        )
      ],
    );
  }

  Widget widArticleImage(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Domaine Carneros", style: TextStyle(fontSize: 30),),
            Text("Le Reve Blanc",  style: TextStyle(fontSize: 30),),
          ],
        ),
        SizedBox(height: 20,),

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Year", style: TextStyle(fontSize: 15),),
            Text("2014",  style: TextStyle(fontSize: 15, fontWeight:  FontWeight.bold),),
          ],
        ),
        SizedBox(height: 20,),

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Country", style: TextStyle(fontSize: 15),),
            Text("French",  style: TextStyle(fontSize: 15, fontWeight:  FontWeight.bold),),
          ],
        ),
        SizedBox(height: 20,),

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Type", style: TextStyle(fontSize: 15),),
            Text("White Dry",  style: TextStyle(fontSize: 15, fontWeight:  FontWeight.bold),),
          ],
        ),
        SizedBox(height: 40,),

        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("\$109.99", style: TextStyle(fontSize: 25, fontWeight:  FontWeight.bold),),
            Container(
              padding: new EdgeInsets.fromLTRB(0, 9, 0, 0),
              child: Text(" /0.2 gal",  style: TextStyle(fontSize: 15, color: Colors.grey),),
            ),

          ],
        ),
      ],
    );
  }

}
