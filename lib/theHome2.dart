import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:komido_boutique/theHome3.dart';
class TheHome2 extends StatefulWidget {
  const TheHome2({Key? key}) : super(key: key);

  @override
  State<TheHome2> createState() => _TheHome2State();
}

class _TheHome2State extends State<TheHome2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text("Skip", style: GoogleFonts.aBeeZee(color: Colors.grey,)),
              ],
            ),
            SizedBox(height: 70),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Image.asset('assets/images/cap2.jpg'),
              ],
            ),
            SizedBox(height: 50),
            Row(
              children: [
                Text("Huge selection of wines for you",
                    style: GoogleFonts.aBeeZee(color: Colors.grey,)),
              ],
            ),
            SizedBox(height: 20),
            Row(

              children: [
                Text("Select wines from", style: GoogleFonts.aBeeZee(
                    fontWeight: FontWeight.bold, fontSize: 30),)
              ],
            ),
            Row(

              children: [
                SizedBox(height: 20),
                Text("different types", style: GoogleFonts.aBeeZee(
                    fontWeight: FontWeight.bold, fontSize: 30),)
              ],
            ), Row(

              children: [
                SizedBox(height: 20),
                Text("ages,countries", style: GoogleFonts.aBeeZee(
                    fontWeight: FontWeight.bold, fontSize: 30),)
              ],
            ),
            SizedBox(height: 75),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 30),
                  child: Row(
                    children: [
                      Text("_", style: GoogleFonts.aBeeZee(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Color(0xFF421927),),),
                      Text("_", style: GoogleFonts.aBeeZee(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Color(0xFF421927),),),
                      Text("_", style: GoogleFonts.aBeeZee(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Colors.grey,),),
                    ],
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        primary: Color(0xFF421927),
                        onPrimary: Colors.white,
                        elevation: 3,
                        minimumSize: Size(130, 60), //////// HERE
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => TheHome3()));
                      },
                      child: Text('Next'),
                    ),
                  ],
                ),
              ],
            )

          ],
        ),
      ),
    );
  }
}
