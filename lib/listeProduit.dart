import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:komido_boutique/detailProduit.dart';

const  cWhite = Color(0xFFFCFDFD);
const  cBlack = Colors.black;


class ListeProduit extends StatefulWidget {
  const ListeProduit({Key? key, required String title}) : super(key: key);

  @override
  State<ListeProduit> createState() => _ListeProduitState();
}

class _ListeProduitState extends State<ListeProduit> {
  final List menuItems = ["Red", "White", "Rose", "Sparkle", "Fortified"];
  List<Color>  _stackColor = [Colors.white, Colors.white,Colors.white,Colors.white,Colors.white,Colors.white,Colors.white,];

  Map<int, List<String>> items = {
    0: ['assets/listVins/1.jpg', 'Domaine Cameros Le Reve Blanc 2014', '\$219.98'],
    1: ['assets/listVins/5.jpg', 'Bellavista Rose Franciacorta 2016', '\$44,99'],
    2: ['assets/listVins/6.jpg', 'L\'Ermitage Brut Rose 2012', '\$79.99'],
    3: ['assets/listVins/8.jpg', 'Giulio Ferrari\' Riserva del Fondatore 2007', '\$149.99'],
    4: ['assets/listVins/1.jpg', 'Giulio Ferrari\' Riserva del Fondatore 2007', '\$149.99'],
    5: ['assets/listVins/6.jpg', 'Giulio Ferrari\' Riserva del Fondatore 2007', '\$149.99'],
    6: ['assets/listVins/8.jpg', 'Giulio Ferrari\' Riserva del Fondatore 2007', '\$149.99'],
  };
  Map<int, List<String>> items1 = {
    0: ['assets/listVins/1.jpg', 'Domaine Cameros Le Reve Blanc 2014', '\$219.98'],
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading:IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey,
            size: 29,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ) ,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0,
        elevation: 0,
        title: Text(
          "What Would You Like?",
          style: GoogleFonts.inter(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        //padding: const EdgeInsets.only(left: 10, right: 10),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: [
                  widSearchBar(),
                  SizedBox(height: 25,),
                  widInfoVin(),
                ],
              ),
            ),
            SizedBox(height: 30,),
            widCategory(),
            SizedBox(height: 30,),
            Container(
              padding: new EdgeInsets.all(20),
              child:widListWins(),
            )

          ],
        ),
      ),
      bottomNavigationBar: widBottomNavigationBar(),
    );
  }

  Widget widBottomNavigationBar(){
    return SafeArea(
        child: Container(
          padding:  new EdgeInsets.all(12),
          margin: new EdgeInsets.symmetric(horizontal: 24),
          decoration: BoxDecoration(
            color: Color(0xFF421927),
            borderRadius: BorderRadius.all(Radius.circular(24)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 36,
                width: 36,
                child: Icon(Icons.home, color: Colors.white,),
              ),
            SizedBox(
                height: 36,
                width: 36,
                child: Icon(Icons.favorite_outline, color: Colors.white,),
              ),
              SizedBox(
                height: 36,
                width: 36,
                child: Icon(Icons.settings_outlined, color: Colors.white,),
              ),
              SizedBox(
                height: 36,
                width: 36,
                child: Icon(Icons.person_pin, color: Colors.white,),
              )
            ],


          ),
        )
    );
  }

  Widget widListWins(){
    //print("Le conde: ${items[1]![1]}");
    return GridView.builder(
        physics: const BouncingScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 9 / 14,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        itemCount: items.length,
        itemBuilder: (BuildContext ctx, index) {

          return InkWell(
            onTap: () {
              setState(() {
                _stackColor[index] = Color(0xFFFEF5F6);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailProd()));
              });
            },
            child: Container(

              child: Stack(
                children: [
                  Container(
                      decoration: BoxDecoration(
                        color: _stackColor[index],
                        border: Border.all(
                            color: Color(0xFFF3EDF1)
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(20)),

                      ),
                      child: Container(
                        padding: new EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Container(
                                child: Image.asset(items[index]![0],)
                            ),
                            SizedBox(height: 10,),
                            Column(
                              children: [
                                Text(items[index]![1], style: GoogleFonts.aBeeZee(fontSize: 11),),
                                //SizedBox(height: 20,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(items[index]![2], style: GoogleFonts.aBeeZee(fontSize:15, fontWeight: FontWeight.bold),),
                                    IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.add_circle)
                                    )
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                      )
                  ),

                  Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        child: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.favorite_outline,size: 18, color: Colors.black)
                        ),

                      )

                  ),


                ],
              ),
            )

          );
        });
  }

Widget widSearchBar() {
  return InkWell(
    onTap: (){},
    child: Container(
      padding: const EdgeInsets.only(left: 30),
      decoration: const BoxDecoration(
        color: Color(0xFFF6F6F6),
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: TextFormField(
        decoration: const InputDecoration(
          labelText: "Search",
          icon: Icon(Icons.search, size: 25,),
          border: InputBorder.none,
        ),
      ),
    ),
  );
}

Widget widInfoVin() {
  return Container(
    height: 100,
    child: Row(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
          decoration: BoxDecoration(
            color: Color(0xFFFDF4F5),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          child: Column(
            children: [
              Text("Constumer's Choice"),
              Text("Ferghettina Rose 'Eronero'"),
              SizedBox(height: 2,),
              Row(
                children: [
                  Text("\$30.99  "),
                  Text("/0,2 gal"),
                ],
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
              image: DecorationImage(
                  image: AssetImage("assets/avatar/vin.png"),
                  fit: BoxFit.cover
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget widCategory() {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.all(10),
          child: InkWell(
            onTap: (){},
            child: Text(
              "Red",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: InkWell(
            onTap: (){},
            child: Text(
              "White",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: InkWell(
            onTap: (){},
            child: Text(
              "Rose",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Color(0xFFF6F6F6),
              borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: InkWell(
            onTap: (){},
            child: Text(
              "Sparkle",
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: InkWell(
            onTap: (){},
            child: Text(
              "Fortified",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
}