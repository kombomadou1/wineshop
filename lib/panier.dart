import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:komido_boutique/googleMap.dart';

class Panier extends StatefulWidget {
  const Panier({Key? key}) : super(key: key);

  @override
  State<Panier> createState() => _PanierState();
}

class _PanierState extends State<Panier> {
  int _articleNumber=2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading:IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey,
            size: 29,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ) ,
        title: Text("Cart", style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        child:
          ListView(
          children: [
            Container( // BLOC1
              padding: new EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Column(
                //shrinkWrap: true,
                children:[ ListView(
                  shrinkWrap: true,
                  children: [
                    widProduct1(),
                    SizedBox(height: 20,),
                    widProduct2(),
                    SizedBox(height: 20,),
                    widProduct3(),
                    SizedBox(height: 20,),
                    widProduct4(),


                  ],
                )

                ],
              )
            ),

            Container(
              child: widTotalPrice(),
            )
          ],
        ),
      ),
    );
  }

  Widget widTotalPrice(){
    return Container(
      child: Container(
       padding: new EdgeInsets.all(20),
        decoration: BoxDecoration(
          //color: Color(0xFFF4DEE3)
          border: Border(
            top: BorderSide(color: Color(0xFFF4DEE3)),
          )
        ),

        child: Column(
          children: [
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total price", style: GoogleFonts.aBeeZee(color: Colors.black)),
                Text("\$494.95", style: GoogleFonts.aBeeZee(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 17)),
              ],
            ),
            Container(
              padding: new EdgeInsets.fromLTRB(0, 100, 0, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  primary: Color(0xFF421927),
                  onPrimary: Colors.white,
                  elevation: 3,
                  minimumSize: Size(270, 60), //////// HERE
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => GoogleMap()));
              },
                child: Text('Proceed to Checkout'),
              ),
            )
          ],
        ),
      )

        );
  }

  Widget widProduct1(){
    return Stack(
      children: [
          ListTile(
          leading: Stack(
            children: [
            Container(
              //padding: new EdgeInsets.all(30),
              height: 70,
              width: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                border: Border.all(
                  color: Color(0xFFF2EBEE)
                  )
                ),
              child: Image.asset("assets/vins/bouteilleVin6.jpg", scale: 25, ),
              ),
            ],
            ),

          title: Container(
            padding: new EdgeInsets.fromLTRB(10, 0, 45, 0),
            child:Text("Domaine Cameros Le Reve Blanc 2014  je suis dans la place", style: GoogleFonts.aBeeZee(fontSize: 15),) ,
            ),
          subtitle: Container(
            padding: new EdgeInsets.fromLTRB(10, 0, 0, 0),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Text("\$219.98", style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 17),),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                IconButton(
                  onPressed: () {
                    setState(() {
                      _articleNumber--;
                      });
                    },
                    icon: Icon(Icons.remove,size: 13, color: Colors.black)
                    ),

                //SizedBox(width: 20,),
                Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    color: Color(0xFFF4DEE3),
                    borderRadius: BorderRadius.circular(30.0),
                    ),
                  child: Center(
                    child: Text(_articleNumber.toString(), style: GoogleFonts.aBeeZee(fontSize: 13, fontWeight: FontWeight.bold),),
                    ),
                  ),
                //SizedBox(width: 10,),

                IconButton(
                  onPressed: () {
                    setState(() {
                      _articleNumber++;
                      });
                    },
                    icon: Icon(Icons.add,size: 13, color: Colors.black,)
                    ),
                ],
                ),
            ],
            )
          ),
          ),
        Positioned(
          left: 50.0,
          top: 6.0,
          child: Container(
            height: 22.7,
            width: 22.7,
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(30.0),
            ),
            child:  Center(
              child: IconButton(
                  onPressed: () {
                    setState(() {
                      _articleNumber--;
                    });
                  },
                  icon: Icon(Icons.close,size: 8, color: Colors.white)
              ),
              )

            )

        ),


        ],
      );


  }

  Widget widProduct2(){
    return Stack(
      children: [
        ListTile(
          leading: Stack(
            children: [
              Container(
                //padding: new EdgeInsets.all(30),
                height: 70,
                width: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    border: Border.all(
                        color: Color(0xFFF2EBEE)
                    )
                ),
                child: Image.asset("assets/vins/bouteilleVin4.png", scale: 25, ),
              ),
            ],
          ),

          title: Container(
            padding: new EdgeInsets.fromLTRB(10, 0, 45, 0),
            child:Text("Bellavista Rose Franciacorta 2016", style: GoogleFonts.aBeeZee(fontSize: 15),) ,
          ),
          subtitle: Container(
              padding: new EdgeInsets.fromLTRB(10, 0, 0, 0),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("\$44,99", style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 17),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              _articleNumber--;
                            });
                          },
                          icon: Icon(Icons.remove,size: 13, color: Colors.black)
                      ),

                      //SizedBox(width: 20,),
                      Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          color: Color(0xFFF4DEE3),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Center(
                          child: Text(_articleNumber.toString(), style: GoogleFonts.aBeeZee(fontSize: 13, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      //SizedBox(width: 10,),

                      IconButton(
                          onPressed: () {
                            setState(() {
                              _articleNumber++;
                            });
                          },
                          icon: Icon(Icons.add,size: 13, color: Colors.black,)
                      ),
                    ],
                  ),
                ],
              )
          ),
        ),
        Positioned(
            left: 50.0,
            top: 6.0,
            child: Container(
                height: 22.7,
                width: 22.7,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child:  Center(
                  child: IconButton(
                      onPressed: () {
                        setState(() {
                          _articleNumber--;
                        });
                      },
                      icon: Icon(Icons.close,size: 8, color: Colors.white)
                  ),
                )

            )

        ),


      ],
    );


  }

  Widget widProduct3(){
    return Stack(
      children: [
        ListTile(
          leading: Stack(
            children: [
              Container(
                //padding: new EdgeInsets.all(30),
                height: 70,
                width: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    border: Border.all(
                        color: Color(0xFFF2EBEE)
                    )
                ),
                child: Image.asset("assets/vins/bouteilleVin6.jpg", scale: 25, ),
              ),
            ],
          ),

          title: Container(
            padding: new EdgeInsets.fromLTRB(10, 0, 45, 0),
            child:Text("L'Ermitage Brut Rose 2012", style: GoogleFonts.aBeeZee(fontSize: 15),) ,
          ),
          subtitle: Container(
              padding: new EdgeInsets.fromLTRB(10, 0, 0, 0),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("\$79.99", style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 17),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              _articleNumber--;
                            });
                          },
                          icon: Icon(Icons.remove,size: 13, color: Colors.black)
                      ),

                      //SizedBox(width: 20,),
                      Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          color: Color(0xFFF4DEE3),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Center(
                          child: Text(_articleNumber.toString(), style: GoogleFonts.aBeeZee(fontSize: 13, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      //SizedBox(width: 10,),

                      IconButton(
                          onPressed: () {
                            setState(() {
                              _articleNumber++;
                            });
                          },
                          icon: Icon(Icons.add,size: 13, color: Colors.black,)
                      ),
                    ],
                  ),
                ],
              )
          ),
        ),
        Positioned(
            left: 50.0,
            top: 6.0,
            child: Container(
                height: 22.7,
                width: 22.7,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child:  Center(
                  child: IconButton(
                      onPressed: () {
                        setState(() {
                          _articleNumber--;
                        });
                      },
                      icon: Icon(Icons.close,size: 8, color: Colors.white)
                  ),
                )

            )

        ),


      ],
    );


  }

  Widget widProduct4(){
    return Stack(
      children: [
        ListTile(
          leading: Stack(
            children: [
              Container(
                //padding: new EdgeInsets.all(30),
                height: 70,
                width: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    border: Border.all(
                        color: Color(0xFFF2EBEE)
                    )
                ),
                child: Image.asset("assets/vins/bouteilleVin4.png", scale: 25, ),
              ),
            ],
          ),

          title: Container(
            padding: new EdgeInsets.fromLTRB(10, 0, 45, 0),
            child:Text("Giulio Ferrari' Riserva del Fondatore 2007", style: GoogleFonts.aBeeZee(fontSize: 15),) ,
          ),
          subtitle: Container(
              padding: new EdgeInsets.fromLTRB(10, 0, 0, 0),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("\$149.99", style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 17),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              _articleNumber--;
                            });
                          },
                          icon: Icon(Icons.remove,size: 13, color: Colors.black)
                      ),

                      //SizedBox(width: 20,),
                      Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          color: Color(0xFFF4DEE3),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Center(
                          child: Text(_articleNumber.toString(), style: GoogleFonts.aBeeZee(fontSize: 13, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      //SizedBox(width: 10,),

                      IconButton(
                          onPressed: () {
                            setState(() {
                              _articleNumber++;
                            });
                          },
                          icon: Icon(Icons.add,size: 13, color: Colors.black,)
                      ),
                    ],
                  ),
                ],
              )
          ),
        ),
        Positioned(
            left: 50.0,
            top: 6.0,
            child: Container(
                height: 22.7,
                width: 22.7,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child:  Center(
                  child: IconButton(
                      onPressed: () {
                        setState(() {
                          _articleNumber--;
                        });
                      },
                      icon: Icon(Icons.close,size: 8, color: Colors.white)
                  ),
                )

            )

        ),


      ],
    );


  }

}
