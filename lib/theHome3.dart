import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:komido_boutique/listeProduit.dart';
class TheHome3 extends StatefulWidget {
  const TheHome3({Key? key}) : super(key: key);

  @override
  State<TheHome3> createState() => _TheHome3State();
}

class _TheHome3State extends State<TheHome3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text("Skip", style: GoogleFonts.aBeeZee(color: Colors.grey,)),
              ],
            ),
            SizedBox(height:70),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Image.asset('assets/images/cap3.jpg'),
              ],
            ),
            SizedBox(height:50),
            Row(
              children: [
                Text("Install the wine filter quickly",style: GoogleFonts.aBeeZee(color: Colors.grey,)),
              ],
            ),
            SizedBox(height: 20),
            Row(

              children: [
                Text("You can choose",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),)
              ],
            ),
            Row(

              children: [
                SizedBox(height: 20),
                Text("your favorite",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),)
              ],
            ),Row(

              children: [
                SizedBox(height: 20),
                Text("type of wine",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),)
              ],
            ),
            SizedBox(height: 75),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 30),
                  child: Row(
                    children: [
                      Text("_",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30,color: Color(0xFF421927),),),
                      Text("_",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30,color: Color(0xFF421927),),),
                      Text("_",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30,color: Color(0xFF421927),),),
                    ],
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        primary: Color(0xFF421927),
                        onPrimary: Colors.white,
                        elevation: 3,
                        minimumSize: Size(130, 60), //////// HERE
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListeProduit(title: "bien")));
                      },
                      child: Text('Next'),
                    ),
                  ],
                ),
              ],
            )

          ],
        ),
      ),
    );
  }
}
