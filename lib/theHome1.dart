import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:komido_boutique/theHome2.dart';

class TheHome1 extends StatefulWidget {
  const TheHome1({Key? key}) : super(key: key);

  @override
  State<TheHome1> createState() => _TheHome1State();
}

class _TheHome1State extends State<TheHome1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Center(
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("Skip", style: GoogleFonts.aBeeZee(color: Colors.grey,)),
                ],
              ),
              SizedBox(height:70),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Image.asset('assets/images/cap1.jpg'),
                ],
              ),
              SizedBox(height:50),
              Row(
                children: [
                  Text("welcome to the world of wine",style: GoogleFonts.aBeeZee(color: Colors.grey,)),
                ],
              ),
              SizedBox(height: 20),
              Row(

                children: [
                  Text("Here you pick up",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),)
                ],
              ),
              Row(

                children: [
                  SizedBox(height: 20),
                  Text("a drink that fits all",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),)
                ],
              ),Row(

                children: [
                  SizedBox(height: 20),
                  Text("your criteria",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),)
                ],
              ),
              SizedBox(height: 75),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 30),
                    child: Row(
                      children: [
                        Text("_",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30,color: Color(0xFF421927),),),
                        Text("_",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30,color: Colors.grey,),),
                        Text("_",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30,color: Colors.grey,),),
                      ],
                    ),
                  ),

                  Row(
                    children: [

                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                          primary: Color(0xFF421927),
                          onPrimary: Colors.white,
                          elevation: 3,
                          minimumSize: Size(130, 60), //////// HERE
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => TheHome2()));
                        },
                        child: Text('Next'),
                      ),
                    ],
                  ),
                ],
              )

            ],
          ),
        ),
      ),
    );
  }
}
