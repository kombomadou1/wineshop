import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:komido_boutique/theHome1.dart';
class TheEnd extends StatefulWidget {
  const TheEnd({Key? key}) : super(key: key);

  @override
  State<TheEnd> createState() => _TheEndState();
}

class _TheEndState extends State<TheEnd> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.fromLTRB(0, 80, 0, 10),
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(50, 0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('assets/images/cap5.jpg'),
                ],
              ),
            ),
            SizedBox(height:40),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Done!",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 30),),
              ],
            ),
            SizedBox(height:20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Your order is being processed.",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 12),),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Wait for a call from our manager.",style: GoogleFonts.aBeeZee(fontWeight: FontWeight.bold, fontSize: 12),),
              ],
            ),
            SizedBox(height:80),
            Container(
              padding: EdgeInsets.fromLTRB(40, 40, 40, 0),
              child:

              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  primary: Color(0xFF421927),
                  onPrimary: Colors.white,
                  elevation: 3,
                  minimumSize: Size(270, 68), //////// HERE
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => TheHome1()));
                },
                child: Text('To Home page', style: GoogleFonts.aBeeZee(fontSize: 16),),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
